package com.example.se.dao;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

import com.example.se.domain.Person;
import com.example.se.domain.Privilages;

public class DAO implements IDAO {
	private final static String DBURL = "jdbc:hsqldb:hsql://localhost";
	private final static String DBUSER = "sa";
	private final static String DBPASS = "";
	private final static String DBDRIVER = "org.hsqldb.jdbcDriver";

	private Connection connection;
	private Statement statement;
	private String query;
	private SQLQueryGenerator generator;
	private ResultSet result;
	private PersonBuilder personBuilder;
	private PrintWriter out;
	private boolean isConnected = false;

	public DAO() throws Exception {
		Class.forName(DBDRIVER).newInstance();
		generator = new SQLQueryGenerator();
		personBuilder = new PersonBuilder();
		try {
			connection = DriverManager.getConnection(DBURL, DBUSER, DBPASS);
			statement = connection.createStatement();
			isConnected = true;
		} catch (SQLException e) {
			System.err.println("ERROR: Nie można podłączyć się do bazy");
		}
	}

	@Override
	public int insert(Person person) {
		this.query = generator.insert(person);
		int z = 0;
		try {
			if (isConnected == true) {
				z = this.statement.executeUpdate(this.query);
			} else {
				out.print("<h2>ERROR: Databace not connected</h2>");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return z;
	}

	@Override
	public void edit(Person person, String id) {
		this.query = generator.update(person, id);
		try {
			this.statement.executeQuery(this.query);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void delete(String id) {
		this.query = generator.delete(id);
		try {
			this.statement.executeQuery(this.query);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public int getByLogin(String login) {
		int i = 0;
		this.query = generator.selectLogin(login);
		try {
			if (isConnected == true) {
				this.result = this.statement.executeQuery(query);
				result.next();
				i = result.getInt(1);
			} else {
				out.print("<h2>ERROR: Databace not connected</h2>");
			}
		} catch (SQLException e) {
			out.print("Cannot connect to database");
		}
		return i;
	}

	@Override
	public Map<String, Person> getAll() {
		this.query = generator.getAll();
		Map<String, Person> persons = new HashMap<>();
		try {
			this.result = this.statement.executeQuery(this.query);

			while (result.next()) {
				Person person = new Person();
				person.setLogin(result.getString(2));
				person.setPassword(result.getString(3));
				person.setEmail(result.getString(4));
				person.setPrivilages(Privilages.valueOf(result.getString(5)));
				persons.put(result.getString(1), person);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return persons;
	}

	@Override
	public Person get(String login, String password) {
		Person p = null;
		int count = 0;
		this.query = generator.selectLoginPass(login, password);
		if (isConnected == true) {
			try {
				this.result = this.statement.executeQuery(query);
				this.connection.close();

				if (this.result.next()) {
					count = result.getInt(6);
					if (count == 1) {
						p = new Person();
						p = this.personBuilder.build(this.result);
					}
				}
			} catch (Exception e) {
				out.println("invaild login");
			}
		} else {
			out.print("<h2>ERROR: Databace not connected</h2>");
		}
		return p;
	}

	public void setPrintWriter(PrintWriter w) {
		this.out = w;
	}
}
