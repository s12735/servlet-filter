package com.example.se.dao;

import java.sql.SQLException;
import java.util.Map;

import com.example.se.domain.Person;

public interface IDAO {
	
	int insert(Person person);
	void edit(Person person, String id) throws SQLException;
	void delete(String id) throws SQLException;
	int getByLogin(String login) throws SQLException;
	Map<String, Person> getAll();
	Person get(String login, String password) throws SQLException;
}
