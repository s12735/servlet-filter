package com.example.se.filter;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class PremiumPageFilter implements Filter {

	FilterConfig filterConfig = null;

	public void init(FilterConfig filterConfig) throws ServletException {
		this.filterConfig = filterConfig;
	}

	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
			throws IOException, ServletException {

		String login = "<p>Brak uprawnien</p><a href='success.jsp'>Powrot</a>";

		HttpServletResponse rs = (HttpServletResponse) res;
		HttpServletRequest rq = (HttpServletRequest) req;

		PrintWriter out = res.getWriter();
		String sessionAtribute;

		try {
			sessionAtribute = rq.getSession().getAttribute("permision").toString();
		} catch (NullPointerException e) {
			sessionAtribute = "null";
		}

			if (sessionAtribute.equals("Premium") || sessionAtribute.equals("Admin")) {
				chain.doFilter(req, res);
			} else {
				res.getWriter().println(login);
			}
		
	}

	public void destroy() {
		// add code to release any resource
	}
}