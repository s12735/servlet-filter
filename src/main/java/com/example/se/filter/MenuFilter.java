package com.example.se.filter;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class MenuFilter implements Filter {

	FilterConfig filterConfig = null;

	public void init(FilterConfig filterConfig) throws ServletException {
		this.filterConfig = filterConfig;
	}

	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
			throws IOException, ServletException {

		HttpServletResponse rs = (HttpServletResponse) res;
		HttpServletRequest rq = (HttpServletRequest) req;

		PrintWriter out = res.getWriter();
		
		String normalMenu = "<b>MENU:</b><br/><ol>"
				+ "<li><a href='success.jsp'>Home</a></li>"
				+ "<li><a href='profile.jsp'>Profile</a></li>";
		
		String premiumMenu = normalMenu + "<li><a href='premium.jsp'>Premium</a></li>";
		String adminMenu = premiumMenu + "<li><a href='admin.jsp'>Admin</a></li>"; ;
		String end = "<li><a href='logout'> Logout</a></li></ol><hr> </hr>";
		String sessionAtribute;

		try { 
			sessionAtribute = rq.getSession().getAttribute("permision").toString();
		} catch (NullPointerException e) {
			sessionAtribute = "Default";
		}

		switch (sessionAtribute) {
		case "Normal":
			out.println(normalMenu + end);
			break;
		case "Premium":
			out.println(premiumMenu + end );
			break;
		case "Admin":
			out.println(adminMenu + end);
			break;
		default:
			break;
		}
		
		chain.doFilter(req, res);

	}

	public void destroy() {
		// add code to release any resource
	}
}